import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  console.log(fighter);

  const fighterImage = createFighterImage(fighter);

  const fighterName = createElement({ tagName: 'h1', className: 'fighter-preview___image' });
  fighterName.innerHTML = fighter.name;

  const fighterAttack = createElement({ tagName: 'p', className: 'fighter-preview___attack' })
  fighterAttack.innerHTML = fighter.attack;

  const fighterDefense = createElement({ tagName: 'p', className: 'fighter-preview___attack' })
  fighterAttack.innerHTML = fighter.defense;

  const fighterHealth = createElement({ tagName: 'p', className: 'fighter-preview___attack' })
  fighterAttack.innerHTML = fighter.health;

  fighterElement.appendChild(fighterImage);
  fighterElement.appendChild(fighterName);
  fighterElement.appendChild(fighterAttack);
  fighterElement.appendChild(fighterDefense);
  fighterElement.appendChild(fighterHealth);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
