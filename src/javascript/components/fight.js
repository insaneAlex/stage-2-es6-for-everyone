import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  const attackPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);

  if (blockPower >= attackPower) {
    return 0
  }
  return attackPower - blockPower;
}


export function getHitPower(fighter) {
  return fighter.attack * (Math.random() * 1) + 1;
}

export function getBlockPower(fighter) {
  return fighter.defense * (Math.random() * 1) + 1;
}
